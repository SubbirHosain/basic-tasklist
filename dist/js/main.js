$(document).ready(function() {
    $.material.init();

    var task_title = '';
    var task_description = '';

    //title validation
    $("#title").keyup(function() {
        var vall = $(this).val();
        if (vall == '') 
        {
            alert('enter something');
            task_title = '';
        }
        else
        {
            task_title = vall;
            //console.log(task_title);
        }
    });

    //description validation
    $("#description").keyup(function() {
        var vall = $(this).val();
        if (vall == '') 
        {
            alert('enter something');
            task_description = '';
        }
        else
        {
            task_description = vall;
            //console.log(task_description);
        }
    });    


   $("#add-task").click(function(e) {
       e.preventDefault();

       if (task_title == '' || task_description == '') 
       {
            alert('Correct the errors');
       }
       else
       {    
            //var formData = {task_title: task_title,task_description: task_description};
            //console.log(formData);
            $.ajax({
                url: 'php/create.php',
                type: 'post',
                data: {task_title: task_title,task_description: task_description},
                success:function(data){
                    console.log(data);
                    $("#ajax-msg").css('display', 'block').delay(2000).slideUp(300).html(data);
                    //reseting after successfull submission
                    $("#form-reset")[0].reset();
                    //console.log($("#form-reset"));
                }
            })
            
       }

   }); 

   //view tasks-list
   //load task list
   $("#task-list").load("php/read.php");


});


   //in place edit option
   function showEdit(editableObj){
        //console.log(editableObj);
        $(editableObj).css('background', '#ddd');
   }

    function saveToDatabase(editableObj,column,id) {

        //$(editableObj).css("background","#FFF url(loadericon.gif) no-repeat right");
        //vanish the gif
        //setTimeout(function() { $(editableObj).css("background","#FDFDFD");},300); 
         
        //perform update validation
        //var datam = $(editableObj).text();
        //var str = 'column='+column+'&editval='+datam+'&id='+id;
        //console.log(str);
        var datam = $(editableObj).text();
        if(datam == ''){
            alert('something required');
        }
        else
        {
            $.ajax({
                url: 'php/update.php',
                type: 'post',
                data: 'column='+column+'&editval='+editableObj.innerHTML+'&id='+id,
                success:function(data){
                    $(editableObj).css("background","#FDFDFD");
                    $("#ajax-msg").css('display', 'block').delay(2000).slideUp(300).html(data);
                }
            })            
        }

                        
    }

    function deleteTask(taskId) {
        if(confirm("Are You Sure to  Delete this ?"))
        {
            $.ajax({
                url: 'php/delete.php',
                type: 'POST',
                data: {delete: taskId},
                success:function(data){
                    $("#ajax-msg").css('display', 'block').delay(2000).slideUp(300).html(data);
                }
            });

           $("#task-list").load("php/read.php");  
        }

        return false;
    }