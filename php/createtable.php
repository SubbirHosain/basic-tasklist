<?php 

include 'db.php';

//$table_name = 'task_list';

$sql = "CREATE TABLE IF NOT EXISTS `task_list`
          (

            `id_task` INT NOT NULL AUTO_INCREMENT , 
            `task_name` TEXT NOT NULL , 
            `task_description` TEXT NOT NULL , 
            `status` VARCHAR(25) DEFAULT 'Not Completed',
            `task_created` TIMESTAMP NOT NULL , 
            PRIMARY KEY (`id_task`)

          )";

  try {
      $dbh->exec($sql);
      echo "Table Created Successfully";
  } catch (Exception $e) {
       echo "Failed to Create Database Table : ".$e->getMessage();
  }