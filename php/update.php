<?php 
include 'db.php';

if (isset($_POST['column']) && isset($_POST['editval']) && isset($_POST['id'])) {
   
   $task_column = $_POST['column']; //dynamic column
   $update_description = trim($_POST['editval']);
   $task_id = $_POST['id'];


   //wrap crud statement with try catch
   
   try {

        //dynamic task_column
        $sql = "UPDATE `task_list` SET $task_column = ? WHERE id_task = ?";
        $sth = $dbh->prepare($sql);
        $data = array($update_description,$task_id);
        $result = $sth->execute($data);
        $num = $sth->rowCount();
        if($num === 1)
        {
          echo "Task ".$task_column ." Updated";
        }
        else
        {
          echo "No Changes Made!";
        }


   } catch (Exception $e) {
       echo "An error occured".$e->getMessage();
   }


}