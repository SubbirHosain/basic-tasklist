<?php 
include 'db.php';

if (isset($_POST['delete'])) {
   
   $task_id = $_POST['delete'];

   //wrap crud statement with try catch
   
   try {

        //dynamic task_column
        $sql = "DELETE FROM `task_list` WHERE id_task = ?";
        $sth = $dbh->prepare($sql);
        $data = array($task_id);
        $result = $sth->execute($data);
        if($result)
        {
          echo "Task Deleted";
        }


   } catch (Exception $e) {
       echo "An error occured".$e->getMessage();
   }


}