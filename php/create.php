<?php 
include 'db.php';

if (isset($_POST['task_title']) && isset($_POST['task_description'])) {
   
   $task_title = $_POST['task_title'];
   $task_description = $_POST['task_description'];
  // $task_time = time();

   //wrap crud statement with try catch
   
   try {

       $sql = "INSERT INTO `task_list` (task_name,task_description,task_created) VALUES (:name, :des, now()) ";
       $sth = $dbh->prepare($sql);
       //$data = array($task_title,$task_description,now());
       $result = $sth->execute(array(":name"=>$task_title,":des"=>$task_description));

       if($result)
       {
            echo 'Task Added';
       }

   } catch (Exception $e) {
       echo "An error occured".$e->getMessage();
   }


}