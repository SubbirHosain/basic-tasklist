<?php 

include 'db.php';

try {
    
    $sql = "SELECT * FROM `task_list` ";
    $sth = $dbh->prepare($sql);
    $sth->execute();
    $result = $sth->fetchAll(PDO::FETCH_ASSOC);
    $num = $sth->rowCount();

    if($num)
    {
        foreach ($result as $row) {

            //formatting time
            $format_time = strftime("%b %d, %Y",strtotime($row['task_created']));
            ?>
            <tr>
                <td contenteditable="true" onClick="showEdit(this);" onBlur="saveToDatabase(this,'task_name','<?php echo $row['id_task'] ?>');" title ="Click to Edit"  > <?php echo $row['task_name'] ?> </td>

                <td contenteditable="true" onClick="showEdit(this);" onBlur="saveToDatabase(this,'task_description','<?php echo $row['id_task'] ?>');"> <?php echo $row['task_description'] ?> </td>

                <td contenteditable="true" onClick="showEdit(this);" onBlur="saveToDatabase(this,'status','<?php echo $row['id_task'] ?>');"><?php echo $row['status'] ?></td>

                <td><?php echo $format_time ?></td>
                <td>
                    <button type="button" onClick="deleteTask('<?php echo $row['id_task'] ?>')" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span>
                    </button>
                </td>
            </tr>            
       <?php }
    }


} catch (Exception $e) {
    echo "An errror occured ".$e->getMessage();
}