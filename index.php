<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Bootstrap Material Design</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    
    <!-- Material Design fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link rel="stylesheet" type="text/css" href="dist/css/bootstrap-material-design.css">
    <link rel="stylesheet" type="text/css" href="dist/css/ripples.min.css">
    <link rel="stylesheet" href="dist/css/style.css">
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-offset-3 col-md-6">
                <div class="well bs-component">

                    <form class="form-horizontal" id="form-reset" method="post">
                        <fieldset>
                            <legend>Basic Task List</legend>
                            <div class="form-group">
                                <label for="title" class="col-md-2 control-label">Name</label>
                                <div class="col-md-10">
                                    <input type="text" name="title" class="form-control" id="title">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="description" class="col-md-2 control-label">Description</label>
                                <div class="col-md-10">
                                    <input type="text" name="description" class="form-control" id="description">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-2">
                                    <button type="button" class="btn btn-default">Cancel</button>
                                    <button type="submit" id="add-task" class="btn btn-primary">Add</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>
        <!-- App Alert -->
        <div id="ajax-msg" class="alert alert-success"></div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="dist/js/material.min.js"></script>
    <script src="dist/js/main.js"></script>
</body>

</html>
